package database

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/spf13/viper"
	"log"
)


// Connect takes a single string that specifies
// the name of the configuration file without the extension,
// the directory to look for the configuration file, and
// the environment to use for configuration
func NewConnection(runMode string) (db *sql.DB) {

	// Read configuration
	db, err := sql.Open(
		viper.GetString("database." + runMode + ".driver"), // Driver
		viper.GetString("database." + runMode + ".username")+   // User
		":"+
		viper.GetString("database." + runMode + ".password")+  // Password
		"@tcp("+
		viper.GetString("database." + runMode + ".host")+   // Host 
		":"+
		viper.GetString("database." + runMode + ".port")+   // Port
		")/"+
		viper.GetString("database." + runMode + ".name"))   // Database Name
	if err != nil {
		panic(err.Error())
	} else {
		log.Println("Database Connection successful")
	}
	return db
}
