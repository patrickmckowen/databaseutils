package database

import (
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/jinzhu/gorm"
	"github.com/spf13/viper"
	"log"
)




// Connect takes a single string that specifies
// the name of the configuration file without the extension,
// the directory to look for the configuration file, and
// the environment to use for configuration
func NewConnection(runMode string) (db *gorm.DB) {

	// Read configuration
	dbDriver := viper.GetString("database." + runMode + ".dbDriver")
	dbUser := viper.GetString("database." + runMode + ".dbUser")
	dbPassW := viper.GetString("database." + runMode + ".dbPassW")
	dbHost := viper.GetString("database." + runMode + ".dbHost")
	dbPort := viper.GetString("database." + runMode + ".dbPort")
	dbName := viper.GetString("database." + runMode + ".dbName")

	db, err := gorm.Open(dbDriver, dbUser+":"+dbPassW+"@tcp("+dbHost+":"+dbPort+")/"+dbName)

	if err != nil {
		panic(err.Error())
	} else {
		log.Println("Database Connection successful")
	}
	return db
}
